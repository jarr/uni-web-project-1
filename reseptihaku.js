// Tehdään event listener
document.getElementById('reseptiHakuNappi').addEventListener('click', haku);

// Skripti alkaa
function haku(){

    // Luodaan API parametrit
    let hakuteksti = document.getElementById('resepti-input').value;
    let htparsittu = hakuteksti.replace(/,/g, "%2C");
    let hakumaaritys = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/findByIngredients?number=6&ranking=1&fillIngredients=true&ingredients=";
    
    // Haetaan reseptit annetuilla aineksilla
    getData(hakumaaritys + htparsittu)
    .then(data => tulos(data)) // JSON-string from `response.json()` call
    .catch(error => console.error(error));
    

    function getData(url = ``) {
    // Default options are marked with *
    return fetch(url, {
        method: "GET", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, cors, *same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
            "Content-Type": "application/json",
            "X-RapidAPI-Key": "c45d9042fbmsh23b333f2acc73b8p1fdcd8jsn06f6a77b2c5e",
        },
        redirect: "follow", // manual, *follow, error
        referrer: "no-referrer", // no-referrer, *client
    })
    .then(response => response.json()); // parses response to JSON
    }
     
    // Luodaan HTML output muuttujaan

    function tulos(data){
        let output = '';

            for (let i = 0; i < data.length; i++){
            output += `
            <div class="recipe-block">
            `    
            imageurl();
            output += `
            <h3><a target="_blank" rel="noopener noreferrer" href="`+ reseptiLinkki() +`">${data[i]["title"]}</a></h3>`;
            puuttuvatAinekset();
            output += `
            <p>Likes: ` + data[i].likes + `</b>
            </div>`;

            // Linkataan kuva, jos kuvaa ei ole niin teksti sen sijasta.
            function imageurl(){
                try {
                output += `<img src=${data[i]["image"]}></img>`;
                } catch {
                output += `<p>No Image</p>`;
                }
            }

            // Listataan reseptin puuttuvat ainesosat
            function puuttuvatAinekset(){
                if (data[i].missedIngredientCount == 0) {
                    output += `<p>You have all the ingredients!</p>`;
                } else {
                    output += "<p><b>Missing " + data[i].missedIngredientCount + " ingredients:</b></br>"
                    for (let j = 0; j < data[i].missedIngredients.length; j++) {
                        output += data[i].missedIngredients[j].name;
                        // Lisätään pilkku muiden aineosien paitsi viimeisen jälkeen.
                        if (j !== data[i].missedIngredientCount - 1){
                            output += ", ";
                        }
                    }
                    output += "</p>";
                }
            }

            // Luodaan toimiva linkki reseptiin (jotta ei tarvitse tehdä liikaa API kutsuja)
            function reseptiLinkki() {
                let reseptinNimi = data[i].title;
                let reseptinId = data[i].id;
                
                // Muutetaan välilyönnit väliviivoiksi
                let nimiMuutettu = reseptinNimi.replace(/ /g, "-");
                let reseptinLinkki = "https://spoonacular.com/recipes/" + nimiMuutettu + "-" + reseptinId;
                return reseptinLinkki.toLowerCase();
            }
        }
        // Syötetään tulokset HTML:n
        document.getElementById('recipes').innerHTML = output; 
    } // funktio "tulos" loppuu.

}