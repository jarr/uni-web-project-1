document.getElementById("time").innerHTML = "0 hours, 0 minutes" + "<br>until wake up";

let sToday =  new Date();
let hFrom = sToday.getHours();
let mFrom = sToday.getMinutes();
let hWake = Number(document.getElementById("wakeupH").value);
let mWake = Number(document.getElementById("wakeupM").value);
let wakeTime = new Date(sToday.getFullYear(), sToday.getMonth(), sToday.getDate()+1, hWake, mWake);

function sleepTime(){
  let elapsed = wakeTime.getTime() - sToday.getTime();
  return elapsed;
}

let sleepHours = sleepTime();

function toHours(elapsed) {

  let m = Math.floor((elapsed / (1000 * 60)) % 60);
  let h = Math.floor((elapsed / (1000 * 60 * 60)) % 24);

  h = (h < 10) ? "0" + h : h;
  m = (m < 10) ? "0" + m : m;

  return h + " hours, " + m + " minutes";
}

/*
  function refresh() {
    hWake = Number(document.getElementById("wakeupH").value);
    mWake = Number(document.getElementById("wakeupM").value);
    wakeTime = new Date(today.getFullYear(), today.getMonth(), today.getDate()+1, hWake, mWake);
    sleepHours = sleepTime();

  }
*/
function showData(){
  //refresh();
    hWake = Number(document.getElementById("wakeupH").value);
    mWake = Number(document.getElementById("wakeupM").value);
    wakeTime = new Date(today.getFullYear(), today.getMonth(), today.getDate()+1, hWake, mWake);
    sleepHours = sleepTime();
  let hoursHTML = toHours(sleepHours).toString();
  document.getElementById("time").innerHTML = hoursHTML + "<br>until wake up";
  console.log(toHours(sleepHours));
}

document.getElementById('uniNappi').addEventListener('click', showData);