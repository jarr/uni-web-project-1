const map = L.map('map_container').setView([60.169857, 24.938379], 13);
const ul = document.getElementById('event_ul');
let routing_control = null;
let lat = 60.167390;
let lon = 24.931080;

const device_location_btn = document.getElementById('geo')
    .addEventListener('click', getDeviceLocation);

// Get device location and set map accordingly.
function getDeviceLocation() {
    navigator.geolocation.getCurrentPosition((location) => {
        lat = location.coords.latitude;
        lon = location.coords.longitude;
    });
    map.setView([lat, lon], 13);
}

// Limit the amount of objects from MyHelsinki API. We end up requiring all of it.
let limit = 10000;

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

const url = 'http://open-api.myhelsinki.fi/v1/events/?distance_filter=';

// Fetch MyHelsinki data via a php proxy script.
function getEvents() {
    const query_str = '?url=' + encodeURIComponent(url + lat.toString() + ',' + 
        lon.toString() + ',0.02&limit=' + limit.toString());
    fetch('kartta/proxy.php' + query_str)
        .then((response) => response.json())
        .then((events) => {
            createEventUL(events);
            map.setView([lat, lon], 13);
        });
    map.setView([lat, lon], 13);
}

// Create list elements with the fetched MyHelsinki API data and attach an anonymous function to add routes.
function createEventUL(event_data) {
    // MyHelsinki has insane amounts of duplicates, so we'll need to check for them.
    let known_events = [];
    let event_array = event_data.data;
    event_array = checkDates(event_array);
    for (let key = 0; key < event_array.length; key++) {
        if (event_array[key].name.fi == null) {
            continue;
        } else if (known_events.includes(event_array[key].name.fi)) {
            continue;
        } else {
            known_events.push(event_array[key].name.fi);
        }
        const li = document.createElement('li');
        li.classList.add('map_events_li');
        const title_h = document.createElement('h3');
        title_h.innerHTML = `${event_array[key].name.fi}`;
        const start_day_p = document.createElement('p');
        start_day_p.innerHTML = getTime(event_array[key]);
        const genres_p = document.createElement('p');
        genres_p.innerHTML = getTags(event_array[key].tags);
        li.appendChild(title_h);
        li.appendChild(genres_p);
        li.appendChild(start_day_p);
        li.addEventListener('click', () => {
            if (routing_control != null) {
                map.removeControl(routing_control);
            }
            routing_control = L.Routing.control({
                waypoints: [
                    L.latLng(lat, lon),
                    L.latLng([event_array[key].location['lat'], event_array[key].location['lon']])
                ],
                routeWhileDragging: true,
                createMarker: (i, wp, nWps) => {
                    map.setView([event_array[key].location['lat'], event_array[key].location['lon']], 13);
                    if (i == 0) {
                        return L.marker(wp.latLng).bindPopup(`
                            Olet täällä.` 
                            ).openPopup();
                    }
                    if (i == 1) {
                        return L.marker(wp.latLng).bindPopup(`
                            ${event_array[key].name.fi}`
                            ).openPopup();
                    }
                }
            }).addTo(map);
        })
        ul.appendChild(li);
        map.invalidateSize();
        if (known_events.length > 4) {
            break;
        }
    };
}

// The MyHelsinki API does not provide means to filter by dates, so we'll have to do it manually
// by pulling the entire event database, as the default order is by modification day.
function checkDates(event_array) {
    const today = new Date();
    const week_from_now = new Date(Date.now() + (6.04e+8 / 7));
    let new_event_array = [];
    event_array.forEach((item, index) => {
        const event_date = new Date(event_array[index].event_dates.starting_day);
        if (event_date > today && event_date < week_from_now) {
            new_event_array.push(item);
        }
    });
    return new_event_array;
}

// Format time data from MyHelsinki for the list elements.
function getTime(item) {
    let return_string = '';
    const date_time = new Date(item.event_dates.starting_day);
    let days = `${date_time.getDate()}`.padStart(2, 0);
    let months = `${date_time.getMonth() + 1}`.padStart(2, 0);
    let years = `${date_time.getFullYear()}`;
    let hour = `${date_time.getHours()}`.padStart(2, 0);
    let minute = `${date_time.getMinutes()}`.padStart(2, 0);
    return_string += [days, months, years].join('.');
    return_string += ', klo: ';
    return_string += [hour, minute].join(':');
    return return_string;
}

// Format tags from MyHelsinki for the list elements.
function getTags(item) {
    let return_string = '';
    item.forEach((array_item, i) => {
        return_string += array_item.name + ', ';
    });
    if (return_string != '') {
        return_string = return_string.slice(0, -2);
    }
    return (return_string + '.')[0].toUpperCase() + return_string.slice(1) + '.';
}

const address_input_form = document.getElementById('address_input_form');
const find_address_button = document.getElementById('haku')
    .addEventListener('click', findAddress);
const hri_url = 'https://nominatim.openstreetmap.org/search/';

// Fetch lat/long with an address via OSM Nominatim API
function findAddress() {
    const input_text = address_input_form.value;
    if (address_input_form.value != '') {
        fetch(hri_url + input_text + ', Finland' + '?format=json')
            .then((response) => response.json())
            .then((coords) => {
                lat = coords[0].lat
                lon = coords[0].lon
                map.setView([coords[0].lat, coords[0].lon], 13);
            });
    }
}

getEvents();
getDeviceLocation();
